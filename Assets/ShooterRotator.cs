﻿using UnityEngine;
using System.Collections;

public class ShooterRotator : MonoBehaviour {

    private int direction = 1;
    private float angle = 15;
    private Quaternion rotation;
    private float limit = 0.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Time.timeScale = 3f;

        if (transform.localRotation.y <= limit && direction == 1) {
            float currentY = transform.localRotation.eulerAngles.y + angle * Time.deltaTime;
            transform.localRotation = Quaternion.Euler(0, currentY, 0);
            if (transform.localRotation.y > limit) {
                direction = direction * -1;
                transform.localRotation = Quaternion.Euler(0, 49, 0);
            }
        }

        if(transform.localRotation.y <= limit && direction == -1) {
            float currentY = transform.localRotation.eulerAngles.y - angle * Time.deltaTime;
            transform.localRotation = Quaternion.Euler(0, currentY, 0);
            Debug.Log(transform.localRotation);
            if (transform.localRotation.y > limit) {
                direction = direction * -1;
                transform.localRotation = Quaternion.Euler(0, -49, 0);
            }
        }
	}
}
