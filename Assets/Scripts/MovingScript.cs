﻿using UnityEngine;
using System.Collections;

public class MovingScript : MonoBehaviour {


    private const float boundriesX = 8f;
    private const float boundariesY = 4f;
    private TimerUtil timerShoot;

    public GameObject bullet;
    public GameObject shooter1;
    public GameObject shooter2;


    public float speed = 4f;
    // Use this for initialization
    void Start() {
        timerShoot = new TimerUtil(1f);
        shooter2 = GameObject.Find("Shooter2");
    }

    // Update is called once per frame
    void Update() {
        Move();
        timerShoot.Update();

        if (Input.GetKey(KeyCode.Space)) {
            if (timerShoot.IsLimitReached()) {
   
                Instantiate(bullet, shooter1.transform.position, shooter1.transform.localRotation);
                Instantiate(bullet, shooter2.transform.position, shooter2.transform.localRotation);
            }
        }
    }

    private void Move() {
        if (Input.GetKey("w")) {
            if (gameObject.transform.position.y <= boundariesY) {
                gameObject.transform.position += Vector3.up * Time.deltaTime * speed;
            }
        }
        if (Input.GetKey("s")) {
            if (gameObject.transform.position.y >= -boundariesY) {
                gameObject.transform.position += Vector3.down * Time.deltaTime * speed;
            }
        }
        if (Input.GetKey("d")) {
            if (gameObject.transform.position.x <= boundriesX) {
                gameObject.transform.position += Vector3.right * Time.deltaTime * speed;
            }
        }
        if (Input.GetKey("a")) {
            if (gameObject.transform.position.x >= -boundriesX) {
                gameObject.transform.position -= Vector3.right * Time.deltaTime * speed;
            }
        }
    }
}
